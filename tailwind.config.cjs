/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');
const { fontFamily } = require('tailwindcss/defaultTheme');

module.exports = {
	content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
	theme: {
		extend: {
			colors: {
				primary: colors.sky,
			},
			fontFamily: {
				primary: ["'Inter'", ...fontFamily.sans],
			},
		},
	},
	plugins: [],
};
