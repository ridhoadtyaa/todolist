export type DateFormat = (
	date: string,
	locale?: 'en-US' | 'en-GB' | 'es-ES' | 'id-ID' | 'jp-JP' | 'cn-CN',
	config?: Intl.DateTimeFormatOptions
) => string;

const defaultConfig = {
	dateStyle: 'long',
} as Intl.DateTimeFormatOptions;

export const dateFormat: DateFormat = (date: string, locale = 'en-US', config = defaultConfig) => {
	return new Intl.DateTimeFormat(locale, config).format(new Date(date)) as string;
};
