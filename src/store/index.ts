import { TodoType } from 'todolist';
import { atomWithStorage } from 'jotai/utils';

export const listTodo = atomWithStorage<TodoType[] | undefined>('todos', undefined);
