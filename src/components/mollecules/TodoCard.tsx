import { useState } from 'react';
import { TodoType } from 'todolist';
import useTodo from '../../hooks/useTodo';
import { dateFormat } from '../../utils/dateFormat';
import Button from '../atoms/Button';
import Input from '../atoms/Input';
import Textarea from '../atoms/Textarea';
import { Fields } from './FormTodo';
import Modal from './Modal';

const TodoCard: React.FunctionComponent<TodoType> = ({ id, title, date, description }) => {
	const { deleteTodo, updateTodo } = useTodo();
	const [updateModal, setUpdateModal] = useState<boolean>(false);
	const [fields, setFields] = useState<Fields>({
		title,
		description,
	});

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
		const { name, value } = e.target;

		setFields({
			...fields,
			[name]: value,
		});
	};

	const deleteHandler = () => {
		deleteTodo(id);
	};

	const updateHandler = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		updateTodo(id, fields.title, fields.description);
		setUpdateModal(false);
	};

	return (
		<>
			<div className="flex w-full flex-col justify-between border border-slate-300 p-5">
				<div>
					<h5 className="text-lg font-semibold">{title}</h5>
					<p className="text-sm text-slate-400">{dateFormat(date as unknown as string)}</p>
					<p className="mt-5">{description}</p>
				</div>

				<div className="mt-1 flex items-center space-x-3">
					<Button onClick={deleteHandler} className="mt-4 bg-red-500 py-1 px-3 text-sm font-medium text-white focus:ring focus:ring-red-300">
						Delete
					</Button>
					<Button
						onClick={() => setUpdateModal(true)}
						className="mt-4 bg-green-500 py-1 px-3 text-sm font-medium text-white focus:ring focus:ring-green-300"
					>
						Edit
					</Button>
				</div>
			</div>

			<Modal show={updateModal} onClose={() => setUpdateModal(false)} title="Update Todo" className="">
				<form onSubmit={updateHandler}>
					<div>
						<Input onChange={handleChange} className="mt-4" placeholder="Input your todo" maxLength={50} value={fields.title} name="title" required />
						<p className={`mt-1 text-sm ${title.length === 50 && 'text-red-500'}`}>Character left: {50 - fields.title.length}</p>
					</div>
					<Textarea
						onChange={handleChange}
						name="description"
						className="mt-6 h-auto resize-none"
						rows={5}
						placeholder="Description"
						value={fields.description}
						required
					></Textarea>

					<div className="flex items-center space-x-4">
						<Button type="submit" className="mt-4 bg-primary-500 py-2 px-4 font-medium text-white focus:ring focus:ring-primary-300">
							Update
						</Button>
						<Button
							type="button"
							onClick={() => setUpdateModal(false)}
							className="mt-4 bg-red-500 py-2 px-4 font-medium text-white focus:ring focus:ring-red-300"
						>
							Cancel
						</Button>
					</div>
				</form>
			</Modal>
		</>
	);
};

export default TodoCard;
