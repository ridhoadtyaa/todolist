import { useCallback, useState } from 'react';
import useTodo from '../../hooks/useTodo';
import Button from '../atoms/Button';
import Input from '../atoms/Input';
import Textarea from '../atoms/Textarea';

export type Fields = {
	title: string;
	description: string;
};

const FormTodo: React.FunctionComponent = () => {
	const { addTodos } = useTodo();
	const [fields, setFields] = useState<Fields>({
		title: '',
		description: '',
	});

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
		const { name, value } = e.target;

		setFields({
			...fields,
			[name]: value,
		});
	};

	const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		addTodos({
			id: Date.now(),
			date: Date.now(),
			title: fields.title,
			description: fields.description,
		});

		setFields({ title: '', description: '' });
	};

	return (
		<form onSubmit={submitHandler}>
			<div>
				<Input className="mt-4" placeholder="Input your todo" maxLength={50} value={fields.title} name="title" onChange={handleChange} required />
				<p className={`mt-1 text-sm ${fields.title.length === 50 && 'text-red-500'}`}>Character left: {50 - fields.title.length}</p>
			</div>
			<Textarea
				name="description"
				className="mt-6 h-auto resize-none"
				rows={5}
				placeholder="Description"
				value={fields.description}
				onChange={handleChange}
				required
			></Textarea>

			<Button type="submit" className="mt-4 bg-primary-500 py-2 px-4 font-medium text-white focus:ring focus:ring-primary-300">
				Create
			</Button>
		</form>
	);
};

export default FormTodo;
