const Header: React.FunctionComponent = () => {
	return (
		<header className="fixed top-0 w-full bg-white">
			<nav className="layout border-b border-slate-200  py-4">
				<h2 className="text-center text-primary-500">ToDoList</h2>
			</nav>
		</header>
	);
};

export default Header;
