import { TodoType } from 'todolist';
import useTodo from '../../hooks/useTodo';
import TodoCard from '../mollecules/TodoCard';

const TodoList: React.FunctionComponent = () => {
	const { todos } = useTodo();

	return (
		<section className="my-20">
			<h3>Your To Do List</h3>

			{todos?.length ? (
				<div className="mt-6 grid grid-cols-1 gap-4 md:grid-cols-2">
					{todos
						.sort((a, b) => a.date - b.date)
						.map((todo: TodoType) => (
							<TodoCard key={todo.id} {...todo} />
						))}
				</div>
			) : (
				<p className="mt-6 text-center">The to-do list hasn't been created yet</p>
			)}
		</section>
	);
};

export default TodoList;
