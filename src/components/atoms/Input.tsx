import { twMerge } from 'tailwind-merge';

type InputProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

const Input: React.FunctionComponent<InputProps> = ({ type = 'text', className, ...props }) => {
	return (
		<input
			type={type}
			className={twMerge('w-full border border-primary-500 px-3 py-1 focus:outline-none focus:ring-1 focus:ring-primary-500', className)}
			{...props}
		/>
	);
};

export default Input;
