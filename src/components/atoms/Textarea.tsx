import { createElement } from 'react';
import { twMerge } from 'tailwind-merge';

type TextAreaProps = React.DetailedHTMLProps<React.TextareaHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement>;

const Textarea: React.FunctionComponent<TextAreaProps> = ({ className, ...props }) => {
	return createElement('textarea', {
		...props,
		className: twMerge('w-full border border-primary-500 px-3 py-1 focus:outline-none focus:ring-1 focus:ring-primary-500', className),
	});
};

export default Textarea;
