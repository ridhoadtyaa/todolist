import FormTodo from './components/mollecules/FormTodo';
import Header from './components/organism/Header';
import TodoList from './components/organism/TodoList';

function App() {
	return (
		<>
			<Header />

			<main className="layout mt-28 scroll-mt-28">
				<section>
					<h2>Create a New Todo</h2>

					<FormTodo />
				</section>

				<TodoList />
			</main>
		</>
	);
}

export default App;
