import { TodoType } from 'todolist';
import { useAtom } from 'jotai';
import { listTodo } from '../store';
import { useCallback } from 'react';

const useTodo = () => {
	const [todos, setTodos] = useAtom(listTodo);

	const addTodos = useCallback(
		(fields: TodoType) => {
			if (todos?.length) {
				setTodos([...todos, fields]);
			} else {
				setTodos([fields]);
			}
		},
		[todos]
	);

	const deleteTodo = useCallback(
		(todoId: number) => {
			const filteredTodo = todos?.filter(todo => todo.id !== todoId);

			setTodos(filteredTodo);
		},
		[todos]
	);

	const updateTodo = useCallback(
		(todoId: number, title: string, description: string) => {
			const findTodo = todos?.find(todo => todo.id === todoId);
			const filteredTodo = todos?.filter(todo => todo.id !== todoId);

			if (findTodo) {
				findTodo.title = title;
				findTodo.description = description;

				setTodos([...(filteredTodo as TodoType[]), findTodo]);
			}
		},
		[todos]
	);

	return {
		todos,
		addTodos,
		deleteTodo,
		updateTodo,
	};
};

export default useTodo;
