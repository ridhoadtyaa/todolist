declare module 'todolist' {
	export type TodoType = {
		id: number;
		title: string;
		date: number;
		description: string;
	};
}
